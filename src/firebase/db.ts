import { db } from "./firebase";
import { IComments } from "../models/comment";

// User API
export const doSetComment = (comment: IComments) =>
  db.ref(`comments/`).push({
    date: comment.date,
    author_name: comment.author_name,
    author_mail: comment.author_mail,
    text: comment.text
  });

export const onceGetComments = () => db.ref("comments");