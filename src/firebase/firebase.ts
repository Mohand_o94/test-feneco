import * as firebase from "firebase/app";
import "firebase/database";


const config = {
  apiKey: "AIzaSyB2MaMEY01dV4fU76IenuPBQ3WfQruXOEA",
    authDomain: "test-cf508.firebaseapp.com",
    databaseURL: "https://test-cf508.firebaseio.com",
    projectId: "test-cf508",
    storageBucket: "test-cf508.appspot.com",
    messagingSenderId: "221390625965"
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

export const db = firebase.database();
