import { COMMENTS_SET, COMMENTS_LIMIT_SET } from "../contants/action";

export function setComment(result: any) {
    return {
      type: COMMENTS_SET,
      payload: result
    };
  }

  export function setCommentLimit(result: any) {
    return {
      type: COMMENTS_LIMIT_SET,
      payload: result
    };
  }  