import { COMMENTS_SET } from "../contants/action";

const INITIAL_STATE = {
    comments: null,
  };
  
  const applySetComments = (state: any, action: any) => ({
    ...state,
    comments: action.payload,
  });
  
  function commentReducer(state = INITIAL_STATE, action: any) {
    switch (action.type) {
      case COMMENTS_SET: {
        return applySetComments(state, action);
      }
      default:
        return state;
    }
  }
  
  export default commentReducer;
  