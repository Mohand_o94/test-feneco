import { combineReducers } from 'redux';
import commentReducer from './comment';

const rootReducer = combineReducers({
  commentState: commentReducer
});

export default rootReducer;