export interface IComments {
    date: string,
    author_name: string,
    author_mail: string,
    text: string
}