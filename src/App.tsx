import React from 'react';
import './App.css';
import Comment from './components/comment'

const App: React.FC = () => {
  return (
    <div className="App">
      <Comment />
    </div>
  );
}

export default App;
