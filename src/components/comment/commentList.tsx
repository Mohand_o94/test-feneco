import React from 'react';
import { IComments } from '../../models/comment';
import { Table } from 'react-bootstrap';
import './style.css';

//const s = require("./style.scss");

interface IOwnProps {
    commentList: Array<IComments>;
}

type ICommentL = IOwnProps;

class CommentList extends React.Component<ICommentL>{

    render() {
        const { commentList } = this.props;
        if (commentList != null) {
            return (
                <div>
                    <h2>Comments</h2>
                    <Table striped responsive>
                        <tbody>
                            {Object.keys(commentList).map((key) => {
                                return (
                                    <tr>
                                        <td>
                                            {getFormatDate(commentList[key as any].date)}
                                            <br />
                                            {'By '}
                                            <a href={"mailto:" + commentList[key as any].author_mail}>
                                                {commentList[key as any].author_name}
                                            </a>
                                        </td>
                                        <td colSpan={2}>
                                            {commentList[key as any].text}
                                        </td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </Table>
                </div>
            );
        }
        else {
            return 'no comment';
        }
    }

}

function getFormatDate(date: string) {
    const toDate = new Date(date);
    return new Intl.DateTimeFormat('en-GB', {
        year: 'numeric',
        month: 'long',
        day: '2-digit'
    }).format(toDate);

}

export default CommentList;