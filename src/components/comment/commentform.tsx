import React from 'react';
import { IComments } from '../../models/comment';
import { Button, Form } from 'react-bootstrap';

import { db } from '../../firebase';

interface IOwnProps {
    
}

interface IState {
    author_mail: string;
    author_name: string;
    text: string;
}

type ICommentForm = IOwnProps;

class CommentForm extends React.Component<ICommentForm, IState>{
    constructor(props: IOwnProps) {
        super(props);

        this.state = { ...this.INITIAL_STATE };
    }

    private INITIAL_STATE = {
        author_mail: "",
        author_name: "",
        text: ""
    };

    handleConfirmationSubmit = async (event: any) => {
        event.preventDefault();

        const { author_mail, author_name, text } = this.state;

        const comment: IComments = {
            date: Date(),
            author_mail: author_mail,
            author_name: author_name,
            text: text
        };
        db.doSetComment(comment);

        this.setState(this.INITIAL_STATE);


    }

    private static propKey(propertyName: string, value: any): object {
        return { [propertyName]: value };
    }

    private setStateWithEvent(event: any, columnType: string): void {
        this.setState(CommentForm.propKey(columnType, (event.target as any).value));
    }

    render() {
        const { author_mail, author_name, text } = this.state;

        return (
            <div className='form'>
                <h2>Leave a Comment</h2>
                <Form onSubmit={this.handleConfirmationSubmit}>
                <Form.Group controlId="formBasicName">
                        <Form.Label>Your Name</Form.Label>
                        <Form.Control
                            type="name"
                            placeholder="Your name"
                            value={author_name}
                            onChange={(event: any) => this.setStateWithEvent(event, "author_name")}
                            required
                        />
                    </Form.Group>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Your Email</Form.Label>
                        <Form.Control
                            type="email"
                            placeholder="Your email"
                            value={author_mail}
                            onChange={(event: any) => this.setStateWithEvent(event, "author_mail")}
                            required
                        />
                    </Form.Group>
                    
                    <Form.Group controlId="formBasicText">
                        <Form.Label>Comment</Form.Label>
                        <Form.Control
                            as="textarea" rows="6"
                            type="text"
                            placeholder="text"
                            value={text}
                            onChange={(event: any) => this.setStateWithEvent(event, "text")}
                            required
                        />
                    </Form.Group>
                    <Button variant="danger" type="submit">
                        Submit comment
                    </Button>
                </Form>
            </div>
        );
    }
}

export default CommentForm;