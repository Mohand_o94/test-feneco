import React from 'react';
import { IComments } from '../../models/comment';
import { setComment } from '../../actions/comment';
import { connect } from "react-redux";
import { db } from '../../firebase';
import CommentForm from './commentform';
import CommentList from './commentList';
import { Container } from 'react-bootstrap';

interface IStateProps {
    comments: Array<IComments>
}

interface IDispatchProps {
    setComment: typeof setComment
}

type IComment = IStateProps & IDispatchProps;

class Comment extends React.Component<IComment>{

    componentDidMount() {
        db.onceGetComments().on('value',(snapshot: any) => {
            this.props.setComment(snapshot.val());
        });
    }

    render() {
        const {comments} = this.props;

        return (
            <Container> 
                <CommentList commentList={comments} />
                <CommentForm />
            </Container>

        );
    }
}

function mapStateToProps(state: any): IStateProps {
    return {
        comments: state.commentState.comments
    };
}

function mapDispatchToProps(dispatch: any): IDispatchProps {
    return {
        setComment: (result: any) => dispatch(setComment(result))
    };
}

export default connect<IStateProps,IDispatchProps>(mapStateToProps, mapDispatchToProps)(Comment);